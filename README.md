# Design Patterns in C++

## Docs

* https://infotraining.bitbucket.io/cpp-dp
* https://gitpitch.com/infotraining-dev/cpp-dp-slides/master?grs=bitbucket#/

## PROXY

### Proxy settings ###

* add to `.profile`

```
export http_proxy=http://xxx.xxx.xxx.xxx:port
export https_proxy=http://xxx.xxx.xxx.xxx:port
```

## CMAKE command

* cmake .. -DCMAKE_TOOLCHAIN_FILE="/usr/share/vcpkg/scripts/buildsystems/vcpkg.cmake"

## ANKIETA

* https://www.infotraining.pl/ankieta/cpp-dp-2019-09-18-kp
