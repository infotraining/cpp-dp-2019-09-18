#include "starbugs_coffee.hpp"
#include <memory>
#include <cassert>

void client(std::unique_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename Component>
    CoffeeBuilder& add()
    {
        coffee_ = std::make_unique<Component>();

        return *this;
    }

    template <typename Condiment>
    CoffeeBuilder& with()
    {
        assert(coffee_ != nullptr);

        coffee_ = std::make_unique<Condiment>(std::move(coffee_));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
//    std::unique_ptr<Coffee> cf =
//            std::make_unique<Whipped>(
//                std::make_unique<Whisky>(
//                    std::make_unique<Whisky>(
//                        std::make_unique<Whisky>(
//                            std::make_unique<Espresso>()))));

    CoffeeBuilder cb;
    cb.add<Espresso>()
        .with<Whisky>().with<Whipped>();

    client(cb.get_coffee());
}
