#include "bitmap.hpp"
#include <algorithm>
#include <array>
#include <deque>

using namespace std;

struct Bitmap::BitmapImpl
{
    std::deque<uint8_t> image_;
    size_t size;
};

Bitmap::Bitmap(size_t size, char fill_char)
    : impl_(std::make_unique<BitmapImpl>())
{    
    impl_->image_.resize(size);
    std::fill(impl_->image_.begin(), impl_->image_.end(), fill_char);
}

Bitmap::~Bitmap()
{
}

void Bitmap::draw()
{
    cout << "Image: ";
    for (size_t i = 0; i < impl_->image_.size(); ++i)
        cout << impl_->image_[i];
    cout << endl;
}
