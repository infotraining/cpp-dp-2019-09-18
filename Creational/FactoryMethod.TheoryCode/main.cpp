#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <functional>

#include "factory.hpp"

using namespace std;

namespace Classic
{
    class Service
    {
        shared_ptr<LoggerCreator> creator_;

    public:
        Service(shared_ptr<LoggerCreator> creator) : creator_(creator)
        {
        }

        virtual ~Service() = default;

        Service(const Service&) = delete;
        Service& operator=(const Service&) = delete;

        void use()
        {
            unique_ptr<Logger> logger = creator_->create_logger();
            logger->log("Client::use() has been started...");
            run();
            logger->log("Client::use() has finished...");
        }
    protected:
        virtual void run() {}
    };

    using LoggerFactory = std::unordered_map<std::string, shared_ptr<LoggerCreator>> ;

}

namespace Modern
{
    using LoggerCreator = std::function<std::unique_ptr<Logger>()>;

    class Service
    {
        LoggerCreator log_creator_;

    public:
        Service(LoggerCreator creator) : log_creator_(creator)
        {
        }

        virtual ~Service() = default;

        Service(const Service&) = delete;
        Service& operator=(const Service&) = delete;

        void use()
        {
            unique_ptr<Logger> logger = log_creator_();
            logger->log("Client::use() has been started...");
            run();
            logger->log("Client::use() has finished...");
        }
    protected:
        virtual void run() {}
    };

    using LoggerFactory = std::unordered_map<std::string, LoggerCreator> ;

}

template <typename Container>
void print(const Container& vec)
{
    for(const auto& item : vec)
    {
        cout << item << " ";
    }
    cout << "\n";

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        const auto& item = *it;
        cout << item << "\n";
    }
}

namespace Classic
{
    int main_classic()
    {
        LoggerFactory logger_factory;
        logger_factory.insert(make_pair("ConsoleLogger", make_shared<ConsoleLoggerCreator>()));
        logger_factory.insert(make_pair("FileLogger", make_shared<FileLoggerCreator>("data.log")));
        logger_factory.insert(make_pair("DbLogger", make_shared<DbLoggerCreator>()));

        Service srv(logger_factory.at("DbLogger"));
        srv.use();

        return 0;
    }
}

int main()
{
    using namespace Modern;

    LoggerFactory logger_factory;
    logger_factory.insert(make_pair("Console", &std::make_unique<ConsoleLogger>));
    logger_factory.insert(make_pair("FileLogger", [] { return std::make_unique<FileLogger>("data.log"); }));
    logger_factory.insert(make_pair("DbLogger", &std::make_unique<DbLogger>));

    Service srv(logger_factory.at("DbLogger"));
    srv.use();
}
