#include <cassert>
#include <iostream>
#include <memory>
#include <typeinfo>

using namespace std;

class Engine
{
protected:
    virtual Engine* do_clone() const = 0;
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    Engine* clone() const
    {
        Engine* cloned_engine = do_clone();
        assert(typeid (*cloned_engine) == typeid(*this));

        return cloned_engine;
    }

    virtual ~Engine() = default;
};

template <typename EngineType, typename EngineBase = Engine>
class CloneableEngine : public EngineBase
{
protected:
    Engine* do_clone() const override
    {
        return new EngineType(static_cast<const EngineType&>(*this));
    }
};

class Diesel : public CloneableEngine<Diesel>
{
public:
    virtual void start() override
    {
        cout << "Diesel starts\n";
    }

    virtual void stop() override
    {
        cout << "Diesel stops\n";
    }
};

class TDI : public CloneableEngine<TDI, Diesel>
{
public:
    virtual void start() override
    {
        cout << "TDI starts\n";
    }

    virtual void stop() override
    {
        cout << "TDI stops\n";
    }
};        

class Hybrid : public CloneableEngine<Hybrid>
{
public:
    virtual void start() override
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop() override
    {
        cout << "Hybrid stops\n";
    }
};

class LPG : public CloneableEngine<LPG>
{
public:
    virtual void start() override
    {
        cout << "LPG starts\n";
    }

    virtual void stop() override
    {
        cout << "LPG stops\n";
    }
};

class Car
{
    Engine* engine_;

public:
    Car(Engine* engine) : engine_{ engine }
    {
    }

    Car(const Car& source)
        : engine_(source.engine_->clone())
    {
    }

    Car& operator=(const Car& source)
    {
        Car temp(source);
        swap(temp);

        return *this;
    }

    void swap(Car& other)
    {
        std::swap(engine_, other.engine_);
    }

    ~Car()
    {
        delete engine_;
    }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1{ new TDI() };
    c1.drive(100);

    c1 = c1;

    cout << "\n";

    Car c2 = c1;
    c2.drive(200);
}
